FROM julienlecomte/docker-make

RUN apk add --no-cache \
      py-pip \
      python \
    && pip install --upgrade --no-cache-dir setuptools wheel \
    && true ;

RUN apk add --no-cache --virtual pip-molecule-deps \
      gcc \
      libffi-dev \
      musl-dev \
      openssl-dev \
      python-dev \
    && pip install docker molecule \
    && apk del pip-molecule-deps \
    && true ;
