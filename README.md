# docker-molecule

A Docker image that contains `molecule` for Ansible unit testing of roles on a GitLab CI.

## Building

Building is as easy as just running `make`, with tag being the branch name:

~~~bash
make
~~~

## Downloading

`docker-molecule` can be downloaded from Docker Hub or GitLab:

~~~bash
docker pull julienlecomte/docker-molecule
# or
docker pull registry.gitlab.com/jlecomte/images/docker-molecule
~~~

## Contents

This docker inherits from [`julienlecomte/docker-make`](https://gitlab.com/jlecomte/images/docker-make) which inherits itself from [`docker:git`](https://hub.docker.com/_/docker) and adds requirements for running *molecule* on a *GitLab CI*.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Locations

  * GitLab: [https://gitlab.com/jlecomte/images/docker-molecule](https://gitlab.com/jlecomte/images/docker-molecule)
  * Docker hub: [https://hub.docker.com/r/julienlecomte/docker-molecule](https://hub.docker.com/r/julienlecomte/docker-molecule)

